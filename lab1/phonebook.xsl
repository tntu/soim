<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
<html>
  <body>  
    <div class="wrapper">
      <h1>My Phonebook</h1>
      <table>
        <tr bgcolor="#9acd32">
          <th>Name</th>
          <th>Last Name</th>
          <th>Phone</th>
          <th>Country</th>
          <th>City</th>
          <th>Birthday</th>
        </tr>
        <xsl:for-each select="catalog/cd">
        <tr>
          <td><xsl:value-of select="name"/></td>
          <td><xsl:value-of select="last_name"/></td>
          <td><xsl:value-of select="phone"/></td>
          <td><xsl:value-of select="country"/></td>
          <td><xsl:value-of select="city"/></td>
          <td><xsl:value-of select="birthday"/></td>
        </tr>
        </xsl:for-each>
      </table>
    </div>
  </body>

  <style>
    body {
      display: flex;
      flex-direction: column;
      align-items: center;
    }

    .wrapper {
      display: flex;
      flex-direction: column;
      align-items: center;
      max-width: 700px;
    }

    td {
      text-align: center;
    }

    tr {
      background-color: #eee;
  </style>
</html>

</xsl:template>
</xsl:stylesheet>