import {faker} from '@faker-js/faker'

class Person {
  private name: string;
  private lastName: string;
  private phone: string;
  private country: string;
  private city: string;

  constructor() {
    this.name = faker.name.firstName();
    this.lastName = faker.name.lastName();
    this.phone = faker.phone.phoneNumber();
    this.country = faker.address.country();
    this.city = faker.address.cityName();
  }

  print() {
    console.log(`name: \t\t ${this.name}`)
    console.log(`lastName: \t ${this.lastName}`)
    console.log(`phone: \t\t ${this.phone}`)
    console.log(`country: \t ${this.country}`)
    console.log(`city: \t\t ${this.city}`)

    console.log('\n')
  }
}

function generate() {
  const person = new Person();

  person.print();
}

for (let i = 0; i < 5; i++) {
  generate();
}
