import {atom} from 'recoil';

const DrawerState = atom<boolean>({
  key: 'drawerState',
  default: false,
});

export default DrawerState;
