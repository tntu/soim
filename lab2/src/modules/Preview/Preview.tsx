import Button from '@mui/material/Button';
import React, {useState} from 'react';
import XMLViewer from 'react-xml-viewer'

const Preview: React.FC = () => {
  const [xml, setXml] = useState<string>('');

  const getFile = async () => {
    const [fileHandle] = await window.showOpenFilePicker()

    const file = await fileHandle.getFile();
    const xml = await file.text();

    setXml(xml);
  }

  return (
    <div>
      <Button onClick={getFile}>Select file</Button>

      {xml ? 
        <XMLViewer xml={xml} /> : null
      }
    </div>
  );
};

export default Preview;
