import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import {Theme} from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import Editor from 'components/Editor';
import {IProducts } from 'components/Editor/product.interface';
import {XMLBuilder, XMLParser } from 'fast-xml-parser';
import React, { useCallback, useState } from 'react';
import XMLViewer from 'react-xml-viewer'

const useStyles = makeStyles((theme: Theme) => ({
  root: {},
  header: {
    display: 'flex',
    justifyContent: 'flex-end',
  }
}))

const Edit: React.FC = () => {
  const [xml, setXml] = useState<null | string>(null);
  const [obj, setObj] = useState<null | IProducts>(null);
  const classes = useStyles();

  const handleOpenFile = useCallback(async () => {
    const [fileHandler] = await window.showOpenFilePicker();

    const file = await fileHandler.getFile();
    const xml = await file.text();

    const parser = new XMLParser();
    const obj = parser.parse(xml)

    setObj(obj.root);
    setXml(xml);
  }, []);

  const saveFile = async (data: string) => {
    const fileHandle = await window.showSaveFilePicker({
      suggestedName: 'index.xml',
      types: [
        {
          description: 'index',
          accept: {
            'text/xml': '.xml'
          }
        }
      ],
    });

    const writableStream = await fileHandle.createWritable();

    await writableStream.write(data);
    await writableStream.close();
  }

  const handleSave = async (root: IProducts) => {
    const builder = new XMLBuilder({});
    const xml = builder.build({root});

    setXml(xml);
    await saveFile(xml);
  }

  return (
    <>
      <Box className={classes.header}>
        <Button onClick={handleOpenFile}>Відкрити файл</Button>
      </Box>

      <Editor 
        obj={obj}
        save={handleSave}
      />

      {xml ? 
        <XMLViewer xml={xml} /> : null
      }
    </>
  )
}

export default Edit;
