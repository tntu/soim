// eslint-disable-next-line unicorn/filename-case
import './App.scss';

import {ThemeProvider} from '@mui/styles';
import React from 'react';
import {BrowserRouter, Route, Routes} from 'react-router-dom';
import {RecoilRoot} from 'recoil';

import Layout from './components/Layout/Layout';
import theme from './components/theme';
import Edit from './modules/Edit';
import Preview from './modules/Preview';

const App: React.FC = () => (
  <RecoilRoot>
    <ThemeProvider theme={theme}>
      <BrowserRouter>
        <Layout>
          <Routes>
            <Route path="/editor" element={<Edit />} />
            <Route path="/" element={<Preview />} />
          </Routes>
        </Layout>
      </BrowserRouter>
    </ThemeProvider>
  </RecoilRoot>
);

export default App;
