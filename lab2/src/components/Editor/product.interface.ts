export interface IProduct {
  id: string;
  name: string;
  price: string;
}

export interface IProducts {
  products: IProduct[];
}
