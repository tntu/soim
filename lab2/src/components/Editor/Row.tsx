import DeleteIcon from '@mui/icons-material/Delete';
import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
import {Theme} from '@mui/material/styles'
import TextField from '@mui/material/TextField';
import makeStyles from '@mui/styles/makeStyles';
import React from 'react';
import {Control, UseFormRegister} from 'react-hook-form';

import {IProducts} from './product.interface';

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'flex-end',
    margin: theme.spacing(2),
  },
  field: {
    marginLeft: theme.spacing(4),
    display: 'flex',
    alignItems: 'center',
  },
  textField: {
    marginLeft: theme.spacing(2),
    width: '30vw',
  }
}));

type RowProps = {
  index: number,
  append: () => void,
  remove: (index: number) => void,
  register: UseFormRegister<IProducts>,
  control: Control<IProducts, unknown>,
};

const Row: React.FC<RowProps> = ({index, append, remove, register}) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <Box className={classes.field}>
        <TextField 
          className={classes.textField} 
          placeholder='name'
          {...register(`products.${index}.name`)}
        />
      </Box>
      <Box className={classes.field}>
        <TextField 
          className={classes.textField} 
          placeholder='price'
          {...register(`products.${index}.price`)}
        />
      </Box>

      <Box
        className={classes.field}
      >
        <IconButton
          onClick={() => remove(index)}
        >
          <DeleteIcon />
      </IconButton>
      </Box>
    </Box>
  )
}

export default Row;
