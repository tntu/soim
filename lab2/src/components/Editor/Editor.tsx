import Button from '@mui/material/Button';
import {Theme} from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles'
import React, { useCallback,useEffect } from 'react';
import {useFieldArray, useForm} from 'react-hook-form';

import {IProducts} from './product.interface';
import Row from './Row';

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
  },
}))

type EditorProps = {
  obj: IProducts | null;
  save: (products: IProducts) => void;
}

const Editor: React.FC<EditorProps> = ({obj, save}) => {
  const classes = useStyles();

  const {register, control, handleSubmit} = useForm<IProducts>({
    defaultValues: {
      products: [{
        name: '',
        price: '',
      }]
    }
  });

  const {fields, append, remove} = useFieldArray({
    name: 'products',
    control,
  })

  const handleAppend = useCallback(() => {
    append({name: '', price: ''})
  }, [append])

  const handleRemove = (index: number) => {
    remove(index);
  }

  const onSubmit = useCallback((value: IProducts) => {
    save(value);
  }, [save])

  useEffect(() => {
    if (fields.length === 0) {
      handleAppend();
    }
  }, [fields.length, handleAppend])

  useEffect(() => {
    fields.forEach((_, index) => {
      remove(index);
    })

    obj?.products?.forEach(product => append(product));
  }, [append, obj, remove])

  return (
    <div className={classes.root}>
      <form onSubmit={handleSubmit(onSubmit)}>
        {fields.map((field, index) => (
          <Row 
            key={index} 
            index={index} 
            append={handleAppend} 
            remove={handleRemove}
            register={register}  
            control={control}
          />
        ))}
        <Button 
          onClick={handleAppend}
          fullWidth
        >
          Добавити товар
        </Button>

        <Button 
          fullWidth 
          type="submit"
        >
          Зберегти файл
        </Button>
      </form>
    </div>
  )
}

export default Editor;
