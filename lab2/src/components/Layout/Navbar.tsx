import MenuIcon from '@mui/icons-material/Menu';
import AppBar from '@mui/material/AppBar';
import IconButton from '@mui/material/IconButton';
import {Theme} from '@mui/material/styles';
import Toolbar from '@mui/material/Toolbar';
import makeStyles from '@mui/styles/makeStyles';
import clsx from 'clsx';
import React from 'react';
import {useRecoilState} from 'recoil';

import DrawerState from '../../atoms/drawer';

const useStyles = makeStyles((theme: Theme) => ({
  appBar: {},
  appBarShift: {},
  toolbar: {
    MarginLeft: theme.spacing(4),
    marginRight: theme.spacing(4),
  },
}));

const Navbar: React.FC = () => {
  const classes = useStyles();
  const [drawerState, handleChangeStateDrawer] = useRecoilState(DrawerState);

  const handleOpenDrawer = () => {
    handleChangeStateDrawer(true);
  };

  return (
    <AppBar
      position="static"
      className={clsx(classes.appBar, {
        [classes.appBarShift]: drawerState,
      })}
    >
      <Toolbar className={classes.toolbar}>
        <IconButton
          color="inherit"
          edge="start"
          size="large"
          onClick={handleOpenDrawer}
        >
          <MenuIcon />
        </IconButton>
      </Toolbar>
    </AppBar>
  );
};

export default Navbar;
