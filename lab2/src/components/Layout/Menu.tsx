import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import Typography from '@mui/material/Typography';
import React from 'react';
import {Link as RouterLink} from 'react-router-dom';

import {drawerWidth} from './Sidebar';

const routes = [
  {
    href: '/',
    label: 'Перегляд',
  },
  {
    href: '/editor',
    label: 'Редактор',
  },
];

const Menu: React.FC = () => (
  <List sx={{width: '100%'}}>
    {routes.map(({href, label}) => (
      <ListItem
        key={href}
        button
        component={RouterLink}
        to={href}
        style={{
          width: drawerWidth,
        }}
      >
        <Typography>{label}</Typography>
      </ListItem>
    ))}
  </List>
);

export default Menu;
