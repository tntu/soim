import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import Drawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import {Theme, useTheme} from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import React from 'react';
import {useRecoilState} from 'recoil';

import DrawerState from '../../atoms/drawer';
import Menu from './Menu';

export const drawerWidth = 200;

const useStyles = makeStyles((theme: Theme) => ({
  drawer: {
    width: drawerWidth,
    // FlexShrink: 0,React.ReactChildren
  },
  drawerPaper: {
    with: drawerWidth,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    Padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
  },
}));

const Sidebar: React.FC = () => {
  const [state, setState] = useRecoilState(DrawerState);
  const classes = useStyles();
  const theme = useTheme();

  const closeDrawer = () => {
    setState(false);
  };

  return (
    <Drawer
      className={classes.drawer}
      variant="persistent"
      anchor="left"
      open={state}
      classes={{
        paper: classes.drawerPaper,
      }}
    >
      <div className={classes.drawerHeader}>
        <IconButton onClick={closeDrawer}>
          {theme.direction === 'ltr' ? (
						<ChevronLeftIcon />
					): (
            <ChevronRightIcon />
          )}
        </IconButton>
      </div>

      <Menu />
    </Drawer>
  );
};

export default Sidebar;


// {theme.direction === 'ltr' ?
// <ChevronLeftIcon />
// :
// <ChevronRightIcon />
// )}
