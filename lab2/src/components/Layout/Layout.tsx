import React from 'react';

import Navbar from './Navbar';
import Page from './Page';
import Sidebar from './Sidebar';

type LayoutProps = {
  children: React.ReactChild;
};

const Layout: React.FC<LayoutProps> = ({children}) => (
  <>
    <Navbar />
    <Sidebar />
    <Page>{children}</Page>
  </>
);

export default Layout;
