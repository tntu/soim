import {Theme} from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import clsx from 'clsx';
import React from 'react';
import {useRecoilState} from 'recoil';

import DrawerState from '../../atoms/drawer';
import {drawerWidth} from './Sidebar';

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    root: 'display',
    height: '100%',
    maxWidth: '100%',
    overflow: 'hidden',
    position: 'relative',
    minHeight: '90vh',
  },
  rootShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeInOut,
      duration: theme.transitions.duration.standard,
    }),
    marginLeft: drawerWidth,
    overflow: 'hidden',
  },
  contentWrapper: {
    display: 'flex',
    flex: '1 1 auto',
    paddingTop: theme.spacing(2),
  },
  content: {
    flexGrow: 1,
    display: 'flex',
    flex: '1 1 auto',
    flexDirection: 'column',
    overflow: 'auto',

    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(5),
      marginRight: theme.spacing(5),
    },
    paddingLeft: theme.spacing(3),
    paddingRight: theme.spacing(3),
  },
  contentShift: {
    margin: 0,
    paddingRight: 0,
    overflowX: 'hidden',
  },
}));

type PageProps = {
  children: React.ReactChild;
};

const Page: React.FC<PageProps> = ({children}) => {
  const [drawer] = useRecoilState(DrawerState);
  const classes = useStyles();

  return (
    <div
      className={clsx(
          (classes.root,
          {
            [classes.rootShift]: drawer,
          }),
      )}
    >
      <div className={classes.contentWrapper}>
        <div
          className={clsx(classes.content, {[classes.contentShift]: drawer})}
        >
          {children}
        </div>
      </div>
    </div>
  );
};

export default Page;
