import React, {useCallback} from 'react';

type FormProps = {
  children: React.ReactNode | React.ReactChild;
  onSubmit: (event: React.FormEvent) => void;
}

const Form: React.FC<FormProps> = ({onSubmit, ...rest}) => {
  const handleSubmit = useCallback((event: React.FormEvent) => {
    event.preventDefault();

    onSubmit(event);
  }, [onSubmit])

  return <form {...rest} onSubmit={handleSubmit} />;
}

export default Form;