module.exports = {
  env: {
    browser: true,
  },
  extends: [
    'react-app',     
    'eslint:recommended',
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended',
  ],
  parser: '@typescript-eslint/parser',
  globals: {
    process: true,
  },
  parserOptions: {
    ecmaVersion: 12,
    sourceType: 'module',
  },
  settings: {
    react: {
      version: 'detect',
    },
  },
  plugins: ['@typescript-eslint', 'unicorn', 'simple-import-sort'],
  rules: {
    'no-console': ['warn', {allow: ['error']}],
    //indent: 'off',
    //'react-hooks/rules-of-hooks': 'error',
    //'react-hooks/exhaustive-deps': 'error',
    //'react/prop-types': 'off',
    'no-use-before-define': 'off',
    '@typescript-eslint/no-use-before-define': ['warn'],
    '@typescript-eslint/explicit-module-boundary-types': 'off',
    //'@typescript-eslint/no-unused-vars': 'off',
    //'@typescript-eslint/no-empty-function': 'off',
    'import/no-unresolved': 'off',
    'simple-import-sort/imports': 'error',
'simple-import-sort/exports': 'error',
'unicorn/filename-case': [
      'error',
      {
        cases: {
          camelCase: true,
          pascalCase: true,
        },
      },
    ],

  },
};
